## Zobaidul Kazi 

## As a Full Stack Web Developer

I am currently working as a Full Stack Web Developer. I have used HTML, CSS, Bootstrap, JavaScript, React, NodeJS, Express, and MongoDB. I have also worked on several projects using React and NodeJS.

## About me

I am a Web Developer. I have used HTML, CSS, Bootstrap, JavaScript, React, NodeJS, Express, and MongoDB. I have also worked on several projects using React and NodeJS.

## Skills

```md
Nodejs, Typescript, , JavaScript, React, Nextjs, Express, MongoDB, HTML, CSS, Bootstrap, Tailwind, MaterialUI, System design, Web design..
```

# Contact

[Email](mailto:zobaidulkaziT@gmail.com)
<br />
[Website](https://zobaidul.github.io/)
<br />
[GitHub](https://github.com/zobkazi)
